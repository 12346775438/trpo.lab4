﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TRPO_Lab4.WpfApp
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            if (textBox_n.Text.Length == 0 || textBox_a.Text.Length == 0)
            {
                labelsquare.Content = "Введите число";
            }
        }

        double _a = 0;
        double _n = 0;
        double _result = 0;
        RightPolygon rightPolygon = new RightPolygon();

        private void textBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789".IndexOf(e.Text) < 0;
        }

        public void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                _a = Convert.ToDouble(textBox_a.Text);
                _n = Convert.ToDouble(textBox_n.Text);
                rightPolygon.Square(_a, _n);
                _result = rightPolygon.result;
                labelsquare.Content = "Площадь S: " + Math.Round(_result, 3);
                if (textBox_n.Text.Length == 0 || textBox_a.Text.Length == 0)
                {
                    labelsquare.Content = "Введите число";
                }
            }
            catch
            {
                labelsquare.Content = "Введите число";
            }
        }
    }
}
