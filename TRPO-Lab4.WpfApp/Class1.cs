﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TRPO_Lab4.WpfApp
{
    public class RightPolygon
    {
        public double result = 0;
        public void Square(double a, double n)
        {
            result = n * Math.Pow(a, 2) / (4 * Math.Tan(180 * (Math.PI / 180) / n));
        }

    }
}
