﻿namespace TRPO_Lab3.lib
{
    public class RightPolygon
    {

        public void Square(double a, double n)
        {
            double result = n * Math.Pow(a, 2) / (4 * Math.Tan(180 * (Math.PI / 180) / n));
            Console.WriteLine("Площадь правильного многоугольнка равна: " + Math.Round(result, 3));
        }

    }

}


