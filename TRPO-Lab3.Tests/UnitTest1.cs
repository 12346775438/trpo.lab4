namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {

            double a = 10;
            double n = 6;
            double result = n * Math.Pow(a, 2) / (4 * Math.Tan(180 * (Math.PI / 180) / n));
            Assert.AreEqual(259.808, Math.Round(result, 3));
            Assert.Pass();
        }
        [Test]
        public void Test2()
        {
            double a = 10;
            double n = 6;
            if (a < 0 || n <0)
            {
                Assert.Throws<ArgumentException>(
            () => throw new ArgumentException());
            }
            else
            {
                Assert.Fail();
            }

        }
    }
}